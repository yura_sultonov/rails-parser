namespace :schedule do
  task :parse_page, [:link, :attr_name, :attr_value] => [ :environment ] do |t, args|
    puts "Rake task received arguments link=" + args[:link] + ", attribute_name=" + args[:attr_name] \
      + ", attribute_value=" + args[:attr_value]
    WebsiteParserJob.perform_now(args[:link], args[:attr_name], args[:attr_value])
  end

  task :parse_list, [:link, :attr_name, :attr_value, :page_begin, :page_end] => [ :environment ] do |t, args|
    puts "Rake task received arguments link=" + args[:link] + ", attribute_name=" + args[:attr_name] \
      + ", attribute_value=" + args[:attr_value]
    (args[:page_begin].to_i..args[:page_end].to_i).each { |page|
      puts "page - " + page.to_s
      WebsiteParserJob.perform_now(args[:link].sub("{pagination}", page.to_s), args[:attr_name], args[:attr_value])
    }
  end

  task :database => [ :environment ] do
    puts "Rake task with database items"
    Website.all.each { |website|
      if website.page_begin < website.page_end and website.link.include? "{pagination}"
        (website.page_begin..website.page_end).each{ |page|
          puts "Beginning parse of webpage - " + website.link.sub("{pagination}", page.to_s)
          WebsiteParserJob.perform_now(website.link.sub("{pagination}", page.to_s), website.attr_name, website.attr_value)
        }
      else
        puts "Begin parsing webpage " + website.link
        WebsiteParserJob.perform_now(website.link, website.attr_name, website.attr_value)
      end
    }
  end
end