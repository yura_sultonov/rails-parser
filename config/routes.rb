Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "articles#index"
  get "/list", to: "list#new"
  get "/page", to: "page#new"
  get "/articles", to: "articles#index"
  resources :articles
  resources :page
  resources :list
  resources :log
  resources :websites
end
