# Rails Code Challenge

### *For usage and installation this project, please, read [INSTALL.MD](INSTALL.md)*

## Challenge
You follow the news in the IT world on multiple websites. However, it takes time to follow the news on different resources. You got an idea to create a web-based application that fulfills the following requirements:
- parses headlines of one your favorite website
- stores parsed headlines in database
- lists the saved headlines on the webpage in historical order
  The application does not need to:
- handle authentication or authorization
- be aesthetically pleasing
## Parser
Parser needs to parse the headline and link to the full article at least from one web resource. Parsing can be initiated manually or configured to run automatically by specified schedule.
## Data
The parsed data along with parsed datetime must be saved in database. Database should store only unique records.
## View
The view must display a list of headlines in historical order, the newest first. Clicking to the headline must navigate to external web resource to the full article webpage.
## Installation
This application is designed to be run locally on the Rails development server with a PostgreSQL database.
## Prerequisites
Application must be developed and tested with the following components:
Rails (>= 5.0)
Ruby (>= 2.2)
PostgreSQL (>= 9.1)
RSpec