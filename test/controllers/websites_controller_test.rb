require "test_helper"

class WebsitesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get websites_index_url
    assert_response :success
  end

  test "should get show" do
    get websites_show_url
    assert_response :success
  end

  test "should get new" do
    get websites_new_url
    assert_response :success
  end

  test "should get edit" do
    get websites_edit_url
    assert_response :success
  end

  test "should get delete" do
    get websites_delete_url
    assert_response :success
  end
end
