class AddIndexToArticles < ActiveRecord::Migration[6.1]
  def change
    add_index :articles, [:title, :link], unique: true
  end
end
