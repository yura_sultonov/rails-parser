class CreateWebsites < ActiveRecord::Migration[6.1]
  def change
    create_table :websites do |t|
      t.string :link
      t.string :attr_name
      t.string :attr_value
      t.integer :page_begin
      t.integer :page_end

      t.timestamps
    end
  end
end
