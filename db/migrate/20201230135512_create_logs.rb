class CreateLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :logs do |t|
      t.text :message
      t.integer :level

      t.timestamps
    end
  end
end
