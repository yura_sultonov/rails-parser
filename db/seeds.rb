# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Article.create(title: 'Are you a happy developer? Depends on where you live', link: 'https://thenextweb.com/syndication/2020/12/24/are-you-a-happy-developer-depends-on-where-you-live/')
Article.create(title: 'All I want for Christmas is a length-based search option on Netflix', link: 'https://thenextweb.com/plugged/2020/12/24/all-i-want-for-christmas-is-a-length-based-search-option-on-netflix/')