class WebsiteParserJob < ApplicationJob
  queue_as :default

  def perform(link, attribute_name, attribute_value)
    Log.create!(level: Log::LEVEL_INFO, message: "Start parsing the page of articles "\
      + link + ", with attribute name " + attribute_name + ", attribute value " + attribute_value)
    NetworkUtility.get_elements_by_attribute(link, attribute_name, attribute_value).each { |tag|
      link = NetworkUtility.get_attribute_value_of_tag(tag, 'href')
      title = NetworkUtility.get_content_of_tag(tag)
      article = Article.create(title: title, link: link)
      if article.errors.any?
        Log.create!(level: Log::LEVEL_ERROR, message: "Cannot create article \"" + title + \
          + "\" on link " + link + ", " + article.errors.full_messages[0])
      else
        Log.create!(level: Log::LEVEL_INFO, message: "Article \"" + title + "\" on link " + link + \
          + " created successfully")
      end
    }
  end
end
