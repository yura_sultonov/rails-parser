class LogController < ApplicationController
  def index
    @page = 1
    if params[:page].present?
      @page = params[:page].to_i
    end
    @total = (Log.count + 9) / 10
    @logs = Log.all.limit!(9).offset!((@page - 1) * 10).order("created_at DESC")
  end
end
