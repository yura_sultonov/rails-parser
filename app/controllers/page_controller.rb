class PageController < ApplicationController
  def new
    @external_page = ExternalPage.new
  end

  def create
    @external_page = ExternalPage.new(parser_form_params)

    if @external_page.save
      redirect_to controller: "articles", action: "index"
    else
      render :new
    end
  end

  private
  def parser_form_params
    params.require(:external_page).permit(:link)
  end
end
