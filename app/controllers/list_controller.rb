class ListController < ApplicationController
  def new
    @external_list = ExternalList.new
  end

  def create
    @external_list = ExternalList.new(parser_form_params)

    if @external_list.save
      redirect_to controller: "articles", action: "index"
    else
      render :new
    end
  end

  private
  def parser_form_params
    params.require(:external_list).permit(:link, :page_begin, :page_end, :attribute_name, :attribute_value)
  end
end
