class ArticlesController < ApplicationController
  def index
    @page = 1
    if params[:page].present?
      @page = params[:page].to_i
    end
    @total = (Article.count + 9) / 10
    @articles = Article.all.limit!(9).offset!((@page - 1) * 10).order("created_at DESC")
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to action: "index"
    else
      render :new
    end
  end

  private
  def article_params
    params.require(:article).permit(:title, :link)
  end
end
