class WebsitesController < ApplicationController
  def index
    @page = 1
    if params[:page].present?
      @page = params[:page].to_i
    end
    @total = (Website.count + 9) / 10
    @websites = Website.all.limit!(9).offset!((@page - 1) * 10).order("created_at DESC")
  end

  def show
    @website = Website.find(params[:id])
  end

  def new
    @website = Website.new
  end

  def create
    @website = Website.new(website_form_params)

    if @website.save
      redirect_to controller: "websites", action: "index"
    else
      render :new
    end
  end

  def edit
    @website = Website.find(params[:id])
  end

  def update
    @website = Website.find(params[:id])

    if @website.update(website_form_params)
      redirect_to @website
    else
      render :edit
    end
  end

  def destroy
    @website = Website.find(params[:id])
    @website.destroy

    redirect_to controller: "websites", action: "index"
  end

  private
  def website_form_params
    params.require(:website).permit(:link, :page_begin, :page_end, :attr_name, :attr_value)
  end
end
