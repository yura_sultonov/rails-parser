class ExternalPage
  include ActiveModel::Model

  attr_accessor :link

  attr_reader :article

  validates :link, presence: { :message => "Link to external article cannot be blank" }
  validates :link, format: { with: URI::regexp(%w(http https)), :message => "Provided invalid URL for article link" }

  def save
    if valid? and persist!
      true
    else
      false
    end
  end

  private def persist!
    title = NetworkUtility.get_web_page_title(link)
    unless title
      errors.add(:link, "Cannot connect to this URL")
      return false
    end
    @article = Article.create!(link: link, title: title)
    true
  rescue
    errors.add(:link, "Cannot save article, this article already exists in database")
    false
  end
end
