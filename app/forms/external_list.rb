class ExternalList
  include ActiveModel::Model

  attr_accessor :link, :page_begin, :page_end, :attribute_name, :attribute_value

  attr_reader :article

  validates :link, presence: { :message => "Link to external web page cannot be blank" }
  validates :link, format: { with: URI::regexp(%w(http https)), :message => "Provided invalid URL for web page" }
  validates :page_begin, numericality: { only_integer: true }, allow_blank: true
  validates :page_end, numericality: { only_integer: true }, allow_blank: true
  validate :begin_must_be_before_end_page

  def save
    if valid?
      if page_begin < page_end and link.include? "{pagination}"
        (page_begin..page_end).each { |page|
          puts "page - " + page.to_s
          WebsiteParserJob.perform_later(link.sub("{pagination}", page.to_s), attribute_name, attribute_value)
        }
      else
        WebsiteParserJob.perform_later(link, attribute_name, attribute_value)
      end
      true
    else
      false
    end
  end

  private

  def begin_must_be_before_end_page
    errors.add(:page_begin, "Page begin must be before end page") unless page_begin <= page_end
  end
end
