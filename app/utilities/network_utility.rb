class NetworkUtility

  def self.get_webpage_response(link)
    url = URI.parse(link)
    req = Net::HTTP.new(url.host, url.port)
    req.use_ssl = (url.scheme == 'https')
    path = url.path if url.path.present?
    res = req.request_get(path || '/')
    return res
  end

  def self.get_web_page_title(link)
    res = NetworkUtility.get_webpage_response(link)
    if res.code == "200"
      body = res.body
      if body.scan(/<title>(.*?)<\/title>/)
        return body.scan(/<title>(.*?)<\/title>/)[0][0]
      end
      false
    else
      false
    end
  rescue
    false
  end

  def self.get_elements_by_attribute(link, attribute_name, attribute_value)
    res = NetworkUtility.get_webpage_response(link)
    if res.code == "200"
      body = res.body
      elements = body.scan(/<a.*#{attribute_name}=.#{Regexp.escape(attribute_value)}.*?<\/a>/)
      article_count = elements.size
      Log.create!(level: Log::LEVEL_INFO, message: "Page parsing " + link + " successfully, found " + \
        + article_count.to_s + " " + "article".pluralize(article_count))
      return elements
    else
      Log.create!(level: Log::LEVEL_ERROR, message: "Cannot parse page " + link + ", status code of page - " + res.code)
      []
    end
  rescue
    Log.create!(level: Log::LEVEL_ERROR, message: "Cannot parse page " + link + ", invalid URL")
    []
  end

  def self.get_attribute_value_of_tag(tag, attr)
    return tag.scan(/#{attr}="(.[^"]+)"/)[0][0]
  end

  def self.get_content_of_tag(tag)
    return tag.scan(/<a.*?>(.*)<\/a>/)[0][0]
  end
end
