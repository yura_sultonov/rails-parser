class Log < ApplicationRecord

  LEVEL_INFO = 1
  LEVEL_ERROR = 2


  validates :message, presence: { :message => "Log message cannot be blank"}
  validates :level, presence: { :message => "Log level cannot be blank"}

end
