class Article < ApplicationRecord
  validates :title, presence: { :message => "Article title cannot be blank"}
  validates :link, presence: { :message => "Link to external article cannot be blank"}
  validates :link, format: { with: /(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?\z/ix, :message => "Provided invalid URL for article link"}
  validates_uniqueness_of :title, :scope => :link, :message => "This article already exists in database"
end
