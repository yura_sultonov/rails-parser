# Usage and installation

## Installation
First, check that Ruby, NodeJS and its components are correctly installed and added to your PATH variable.
Fire command prompt and run command:

```ruby -v```

Make sure Rails is installed

```rails -v```

Make sure Yarn is installed

```yarn -v```

If you see Ruby and Rails version and Yarn version, then you are good to start, otherwise setup Ruby On Rails or Yarn on your computer. Once done, now:

1) Clone the repository to local folder or download and extract zip folder from gitlab repo. For cloning repository, run on the terminal ```git clone git@gitlab.com:yura_sultonov/rails-parser.git```
2) Enter to project folder and run on terminal ```bundle install``` for installing all dependencies.
3) Enter ```yarn install``` command on terminal for installing yarn packages like bootstrap, jquery and so on.
4) Edit config/database.yml file and replace your PostgreSQL database name and user credentials.
5) Migrate the database schema with command on terminal ```rake db:migrate```
6) Run Ruby on Rails server on your local machine. Run command ```rails server``` on terminal.

Now, you can access to application through your browser at address 127.0.0.1:3000

## Usage
In our application, we have five elements of menu:
* Logs - for view system messages (events) for background tasks.
* Parse list - opens a form for parsing website manually
* Parse page - form parsing a page and it's title, and stores to the database  
* Create manually - creating article manually by filling link and title
* Websites - list of websites for scheduled parsing on background

### Create manually
For creating an article manually, enter Title and Link to article in the form and click Submit button.

### Parse page
For parsing page, enter an URL of article and click to Submit button. Application itself parses headline of page and store to the database.

### Parse list
For parse the list of articles on the entered page, you must fill the elements of form:
- Link to the webpage. If you want to use pagination functionality, you must enter URL like https://slashdot.org/?page={pagination} and application will replace page number with {pagination} on processing of parse.
- Page begin/end. Start number and end of the pagination, if you don't want to use pagination functionality, you can skip filling this field.
- Attribute name and value. Unique attribute name and value of links for articles (```<a href="article_link" attr_name="attr_value">Article title</a>```).

After filled form, click to Submit button and application will parse list of articles from website and stores to database on the background task (ActiveJob task).

*If you want to see results of background tasks, refer to Logs section*

### Websites
Here you can see list of saved websites list, update them or create new one. Information of websites stores on database like fields of Parse list section.

*For configuring scheduled tasks, read cron tasks section*

## Cron tasks 

There are written Rake tasks for interact with terminal commands:
1) Parse page task - parses webpage and stores articles to database
   
```rake schedule:parse_page['https://habr.com/ru','class','post__title_link']```

2) Parse website task - parses website pages with functionality pagination

```rake schedule:parse_list['https://habr.com/ru/','class','post__title_link',1,2] ```

3) Database task - parses websites which stored on database (maybe with pagination)

```rake schedule:database```

### Setup scheduled tasks
Now we have tasks you can run from the command line, but being the experienced developer that you are, you understand the real goal is to automate yourself out of existence. So how do we run these rake tasks on a schedule?

#### Creating a Crontab
Cron is a unix utility for scheduling and executing commands.

To utilize cron, you create a ```crontab```.

This will create a crontab for your user is none exists, or open your crontab for editing.

#### Calling a Rake Task
Creating a cron job that calls a rake task is almost as easy as typing the rake command into the command line.

The one major caveat is that the cron job runs in a different environment. This means environment variables, even very important ones like PATH, may be completely different. This can cause major headaches in your cron jobs.

The solution is to "assume nothing" in your cron jobs. The most basic step to take here is to always use full paths in your cron jobs:

*The WRONG way:* ```cd ~/projects/parser && rake schedule:database```

*The RIGHT way:* ```cd /Users/you/projects/parser && /usr/local/bin/rake RAILS_ENV=production schedule:database```

To find the full path for your rake executable, run ```which rake```.

#### Timing
The timing of a cron job follows a standard format that, while is a little intimidating at first, is really very easy to dig into. In fact, it’s very much like a time based regex in the following pattern: ```minute hour day month weekday```

A few examples:

__15 * * * *__ - Run on the 15th minute of every hour of the day, every day of the month, every month of the year, every day of the week.

__00,15,30,45 * * * *__ - Run on the 0th, 15th, 30th, and 45th minute of every hour of the day, every day of the month, every month of the year, every day of the week.

#### Bringing it All Together
You’ve jumped through the hoops to make your command cron compliant and figured out the timing, now you can automate your rake tasks.

```crontab -e```

```00 00 * * * cd /Users/you/projects/parser && /usr/local/bin/rake RAILS_ENV=production schedule:database```
